// Derived from https://codepen.io/cassidoo/pen/KRdLvL by Cassidy Williams
// CodePen submissions are MIT Licensed (https://blog.codepen.io/legal/licensing/)
import React from 'react';

export default function Normed_DashLoader({loading, style}: {
  loading: boolean,
  style?: { [_: string]: string }
}) {
  return (
    <div style={style || {}} className="Normed_DashLoader__outer">
      <div className={`Normed_DashLoader__container Normed_DashLoader__container--${loading ? "visible" : "hidden"}`}>
        <div className="Normed_DashLoader__dash Normed_DashLoader__uno"></div>
        <div className="Normed_DashLoader__dash Normed_DashLoader__dos"></div>
        <div className="Normed_DashLoader__dash Normed_DashLoader__tres"></div>
        <div className="Normed_DashLoader__dash Normed_DashLoader__cuatro"></div>
      </div>
    </div>
  )
}