# Dash Loader

A react based loader

Derived from https://codepen.io/cassidoo/pen/KRdLvL by Cassidy Williams

# Theming

Use the following css variables to theme:
* `normed-dashloader-color`: The color of the dashes
* `normed-dashloader-shadow`: The color of the glow

# Using

```
import DashLoader from '@normed/loader/DashLoader';
import '@normed/loader/DashLoader/style.css';

function 
```

# License

Both the original work by Cassidy Williams, and the derivations by Normal Gaussian are MIT license